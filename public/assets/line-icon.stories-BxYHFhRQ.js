import{L as a,M as o}from"./line-icon-4UGpsNUR.js";import"./jsx-runtime-BjgbQsUx.js";import"./index-D2MAbzvX.js";import"./index-DEBVq0NN.js";const c={title:"GZMTR/Line Icon",component:a},n={args:{zhName:"2号线",enName:"Line 2",foregroundColour:o.white,backgroundColour:"#00629B",zhClassName:"rmg-name__zh",enClassName:"rmg-name__en",spanDigits:!0},argTypes:{zhName:{options:["2号线","18号线","APM线","广佛线","佛山2号线","海珠有轨1号线"],control:{type:"inline-radio"}},enName:{options:["Line 2","Line 18","APM Line","Guangfo Line","Foshan Line 2","THZ1"],control:{type:"inline-radio"}},foregroundColour:{options:[o.white,o.black],control:{type:"inline-radio"}},backgroundColour:{control:{type:"color"}},spanDigits:{options:[!0,!1],control:{type:"inline-radio"}}}};var e,r,i;n.parameters={...n.parameters,docs:{...(e=n.parameters)==null?void 0:e.docs,source:{originalSource:`{
  args: {
    zhName: '2号线',
    enName: 'Line 2',
    foregroundColour: MonoColour.white,
    backgroundColour: '#00629B',
    zhClassName: 'rmg-name__zh',
    enClassName: 'rmg-name__en',
    spanDigits: true
  },
  argTypes: {
    zhName: {
      options: ['2号线', '18号线', 'APM线', '广佛线', '佛山2号线', '海珠有轨1号线'],
      control: {
        type: 'inline-radio'
      }
    },
    enName: {
      options: ['Line 2', 'Line 18', 'APM Line', 'Guangfo Line', 'Foshan Line 2', 'THZ1'],
      control: {
        type: 'inline-radio'
      }
    },
    foregroundColour: {
      options: [MonoColour.white, MonoColour.black],
      control: {
        type: 'inline-radio'
      }
    },
    backgroundColour: {
      control: {
        type: 'color'
      }
    },
    spanDigits: {
      options: [true, false],
      control: {
        type: 'inline-radio'
      }
    }
  }
}`,...(i=(r=n.parameters)==null?void 0:r.docs)==null?void 0:i.source}}};const m=["LineIcon"];export{n as LineIcon,m as __namedExportsOrder,c as default};
