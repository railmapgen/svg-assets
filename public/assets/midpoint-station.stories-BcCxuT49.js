import{M as i}from"./midpoint-station-qwpIQuV3.js";import"./jsx-runtime-BjgbQsUx.js";import"./index-D2MAbzvX.js";import"./index-DEBVq0NN.js";const c={title:"GZMTR/Midpoint Station",component:i},o={argTypes:{clockwise:{options:[!0,!1],control:{type:"radio"}},anchorAt:{options:["text","circle"],control:{type:"radio"}}}};var t,n,r;o.parameters={...o.parameters,docs:{...(t=o.parameters)==null?void 0:t.docs,source:{originalSource:`{
  argTypes: {
    clockwise: {
      options: [true, false],
      control: {
        type: 'radio'
      }
    },
    anchorAt: {
      options: ['text', 'circle'],
      control: {
        type: 'radio'
      }
    }
  }
}`,...(r=(n=o.parameters)==null?void 0:n.docs)==null?void 0:r.source}}};const d=["MidpointStation"];export{o as MidpointStation,d as __namedExportsOrder,c as default};
