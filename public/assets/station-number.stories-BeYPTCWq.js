import{S as t}from"./station-number-eWZrPEOP.js";/* empty css              */import"./jsx-runtime-BjgbQsUx.js";import"./index-D2MAbzvX.js";import"./index-DEBVq0NN.js";import"./generic-station-number-Bzo6aN_2.js";const d={title:"GZMTR/Station Number",component:t},o={args:{lineNum:"2",stnNum:"01",strokeColour:"#00629B",size:"md",textClassName:"rmg-name__zh"},argTypes:{strokeColour:{control:{type:"color"}},passed:{options:[!0,!1],control:{type:"radio"}},size:{options:["sm","md","lg"],control:{type:"radio"}},bolderBorder:{options:[!0,!1],control:{type:"radio"}},alwaysShowColouredBorder:{options:[!0,!1],control:{type:"radio"}},useSameScale:{options:[!0,!1],control:{type:"radio"}}}};var r,e,n;o.parameters={...o.parameters,docs:{...(r=o.parameters)==null?void 0:r.docs,source:{originalSource:`{
  args: {
    lineNum: '2',
    stnNum: '01',
    strokeColour: '#00629B',
    size: 'md',
    textClassName: 'rmg-name__zh'
  },
  argTypes: {
    strokeColour: {
      control: {
        type: 'color'
      }
    },
    passed: {
      options: [true, false],
      control: {
        type: 'radio'
      }
    },
    size: {
      options: ['sm', 'md', 'lg'],
      control: {
        type: 'radio'
      }
    },
    bolderBorder: {
      options: [true, false],
      control: {
        type: 'radio'
      }
    },
    alwaysShowColouredBorder: {
      options: [true, false],
      control: {
        type: 'radio'
      }
    },
    useSameScale: {
      options: [true, false],
      control: {
        type: 'radio'
      }
    }
  }
}`,...(n=(e=o.parameters)==null?void 0:e.docs)==null?void 0:n.source}}};const u=["StationNumber"];export{o as StationNumber,u as __namedExportsOrder,d as default};
