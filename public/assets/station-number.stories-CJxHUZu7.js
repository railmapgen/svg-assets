import{S as e}from"./station-number-CbugUQ2t.js";/* empty css              */import"./jsx-runtime-BjgbQsUx.js";import"./index-D2MAbzvX.js";import"./index-DEBVq0NN.js";import"./generic-station-number-Bzo6aN_2.js";const u={title:"FMetro/Station Number",component:e},o={args:{lineNum:"F2",stnNum:"20",strokeColour:"#F5333F",size:"md",textClassName:"rmg-name__zh"},argTypes:{strokeColour:{control:{type:"color"}},passed:{options:[!0,!1],control:{type:"radio"}},size:{options:["sm","md","lg"],control:{type:"radio"}}}};var t,r,n;o.parameters={...o.parameters,docs:{...(t=o.parameters)==null?void 0:t.docs,source:{originalSource:`{
  args: {
    lineNum: 'F2',
    stnNum: '20',
    strokeColour: '#F5333F',
    size: 'md',
    textClassName: 'rmg-name__zh'
  },
  argTypes: {
    strokeColour: {
      control: {
        type: 'color'
      }
    },
    passed: {
      options: [true, false],
      control: {
        type: 'radio'
      }
    },
    size: {
      options: ['sm', 'md', 'lg'],
      control: {
        type: 'radio'
      }
    }
  }
}`,...(n=(r=o.parameters)==null?void 0:r.docs)==null?void 0:n.source}}};const c=["StationNumber"];export{o as StationNumber,c as __namedExportsOrder,u as default};
